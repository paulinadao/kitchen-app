﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Paulina.Kitchen.UnitTests
{
    [TestClass]
    public class ItemTests
    {
        [TestMethod]
        public void Item_Name()
        {
            const string ExpectedName = "blah";
            var item = new Item();
            item.Name = ExpectedName;
            Assert.AreEqual(ExpectedName, item.Name);
        }

       [TestMethod]
       public void Item_Measurement()
       {
           var expectedMeasurement = new Measurement();
           var item = new Item();
           item.Measurement = expectedMeasurement;
           Assert.AreEqual(expectedMeasurement, item.Measurement);
       }
    }
}
