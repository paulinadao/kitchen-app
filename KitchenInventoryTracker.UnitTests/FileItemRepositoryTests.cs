﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KitchenInventoryTracker.UnitTests
{
    using System.IO;

    using Paulina.Kitchen;

    [TestClass]
    public class FileItemRepositoryTests
    {
        /// <summary>
        /// The filename
        /// </summary>
        private const string Filename = "filename.txt";

        private const string UpsertFilename = "upsertfile.txt";

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            File.Delete(Filename);
            File.Delete(UpsertFilename);
        }

        /// <summary>
        /// Tests the file item repository with the upsert command when the
        /// file exists.
        /// </summary>
        [TestMethod]
        public void FileItemRepository_FileExists()
        {
            // Make sure that the file exists.
            using (var stream = File.Create(Filename))
            {
            }

            var expectedCreationTime = File.GetLastWriteTime(Filename);

            // Initialize
            var itemRepository = new FileItemRepository(Filename);
            itemRepository.Initialize();
            
            // Make sure the file still exists.
            Assert.IsTrue(File.Exists(Filename));

            var actualCreationTime = File.GetLastWriteTime(Filename);
            Assert.AreEqual(expectedCreationTime, actualCreationTime);
        }

        /// <summary>
        /// Tests the file item repository with the upsert command when the
        /// file does not exist.
        /// </summary>
        [TestMethod]
        public void FileItemRepository_FileDoesNotExist()
        {
            var itemRepository = new FileItemRepository(Filename);
            itemRepository.Initialize();

            // Verify the file exists.
            Assert.IsTrue(File.Exists(Filename));
        }

        /// <summary>
        /// Tests the Get method with an unknown item name.
        /// </summary>
        [TestMethod]
        public void FileItemRepository_GetUnknown()
        {
            // Call Get with jibberish
            var itemRepository = new FileItemRepository(Filename);
            itemRepository.Initialize();
            var actual = itemRepository.Get("blahblah");

            // Verify null
            Assert.IsNull(actual);
        }

        /// <summary>
        /// Tests the Get method with a known item name.
        /// </summary>
        [TestMethod]
        [DeploymentItem("TestData\\ingredients.txt")]
        public void FileItemRepository_GetKnown()
        {
            var itemRepository = new FileItemRepository("ingredients.txt");
            itemRepository.Initialize();
            var testname = itemRepository.Get("milk");
            
            Assert.IsNotNull(testname);

            Assert.AreEqual("milk", testname.Name);
            Assert.AreEqual(4, testname.Measurement.Quantity);
            Assert.AreEqual(MeasurementType.Ounce, testname.Measurement.MeasurementType);

            var secondtestname = itemRepository.Get("cinnamon");

            Assert.IsNotNull(secondtestname);

            Assert.AreEqual("cinnamon", secondtestname.Name);
            Assert.AreEqual(20, secondtestname.Measurement.Quantity);
            Assert.AreEqual(MeasurementType.Gram, secondtestname.Measurement.MeasurementType);
        }

        [TestMethod]
        [DeploymentItem("TestData\\ingredients.txt")]
        public void FileItemRepository_Update()
        {

            File.Copy("ingredients.txt", UpsertFilename);

            var itemRepository = new FileItemRepository(UpsertFilename);
            itemRepository.Initialize();

            var myItem = itemRepository.Get("cinnamon");

            Assert.AreEqual(20, myItem.Measurement.Quantity);

            myItem.Measurement.Quantity = 15;
            itemRepository.Upsert(myItem);

            var updatedItem = itemRepository.Get("cinnamon");

            Assert.AreEqual(myItem.Name, updatedItem.Name);
            Assert.AreEqual(myItem.Measurement.Quantity, updatedItem.Measurement.Quantity);

            var existingItem = itemRepository.Get("milk");

            Assert.IsNotNull(existingItem);

            Assert.AreEqual("milk", existingItem.Name);
            Assert.AreEqual(4, existingItem.Measurement.Quantity);
            Assert.AreEqual(MeasurementType.Ounce, existingItem.Measurement.MeasurementType);
        }

        [TestMethod]
        [DeploymentItem("TestData\\ingredients.txt")]
        public void FileItemRepository_Add()
        {

            var itemRepository = new FileItemRepository(UpsertFilename);
            itemRepository.Initialize();

            var newItem = new Item();
            newItem.Name = "butter";
            
            newItem.Measurement.Quantity = 10;
            newItem.Measurement.MeasurementType = MeasurementType.Tablespoon;
            itemRepository.Upsert(newItem);

            var testItem = itemRepository.Get("butter");

            Assert.AreEqual(newItem.Name, testItem.Name);
            Assert.AreEqual(newItem.Measurement.Quantity, testItem.Measurement.Quantity);
            Assert.AreEqual(newItem.Measurement.MeasurementType, testItem.Measurement.MeasurementType);

        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        [DeploymentItem("TestData\\ingredients.txt")]
        public void FileItemRepository_NullName()
        {
            var itemRepository = new FileItemRepository(UpsertFilename);
            itemRepository.Initialize();

            var nullItemName = new Item();
            nullItemName.Name = null;

            itemRepository.upsert(nullItemName);

            Assert.IsNull(nullItemName.Name);
        }
    }
}
