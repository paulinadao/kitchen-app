﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KitchenInventoryTracker.UnitTests
{
    using Paulina.Kitchen;

    [TestClass]
    public class ConversionsTests
    {
        [TestMethod]
        public void Conversions_OuncesToTablespoons()
        {
            var answer = Conversions.OuncesToTablespoons(8);

            float expected = 8 * 2;

            Assert.AreEqual(expected, answer);
        }

        [TestMethod]
        public void Conversions_TablespoonsToOunces()
        {
          
            var answer = Conversions.TablespoonsToOunces(100);

            float expected = 100 / 2;

            Assert.AreEqual(expected, answer);
        }
    }
}
