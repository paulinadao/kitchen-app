﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KitchenInventoryTracker.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Paulina.Kitchen;

    [TestClass]
    public class MeasurementTests
    {
        [TestMethod]
        public void Measurement_Quantity()
        {
            const float ExpectedQuantity = 4;
            var measurement = new Measurement();
            measurement.Quantity = ExpectedQuantity;
            Assert.AreEqual(ExpectedQuantity, measurement.Quantity);
        }

        [TestMethod]
        public void Measurement_MeasurementType()
        {
            const MeasurementType ExpectedMeasurementType = MeasurementType.Gram;
            var measurement = new Measurement();
            measurement.MeasurementType = ExpectedMeasurementType;
            Assert.AreEqual(ExpectedMeasurementType, measurement.MeasurementType);
        }

    }
}
