﻿namespace Paulina.Kitchen
{
    public enum MeasurementType
    {
        Gram,
        Ounce,
        Tablespoon,
        Teaspoon,
        FluidOunce,
    }
}