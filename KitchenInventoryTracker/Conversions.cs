﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paulina.Kitchen
{
    public static class Conversions
    {
        public static float OuncesToTablespoons(float ounces)
        {
            float tablespoons = (ounces * 2);
            return tablespoons;
        }

        public static float TablespoonsToOunces(float tablespoons)
        {
            float ounces = (float)(tablespoons / 2);
            return ounces;
        }

        public static float GramsToTablespoons(float gram)
        {
            float tablespoons = (float)(gram/14.3);
            return tablespoons;
        }

        public static float TablespoonsToGrams(float tablespoon)
        {
            float grams = (float)(tablespoon * 14.3);
            return grams;
        }

        public static float TablespoonsToTeaspoons(float tablespoon)
        {
            float teaspoon = (float)(tablespoon / 3);
            return teaspoon;
        }

        public static float TeaspoonsToTablespoons(float teaspoon)
        {
            float tablespoon = (float)(teaspoon * 3);
            return tablespoon;
        }

        public static float OuncesToTeaspoons(float ounces)
        {
            float teaspoon = TablespoonsToTeaspoons(OuncesToTablespoons(ounces));
            return teaspoon;

        }

        public static float TeaspoonsToOunces(float teaspoons)
        {
            float ounces = TablespoonsToOunces(TeaspoonsToTablespoons(teaspoons));
            return ounces;
        }
    }
}