﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paulina.Kitchen
{
    public class Item
    {
        public Item()
        {
  
            Measurement = new Measurement();
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the measurement.
        /// </summary>
        public Measurement Measurement { get; set; }
    }
}
