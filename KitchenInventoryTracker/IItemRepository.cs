﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paulina.Kitchen
{
    public interface IItemRepository
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();

        /// <summary>
        /// Updates or inserts the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void Upsert(Item item);

        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>An item with the specified name.</returns>
        Item Get(string name);

    }
}
