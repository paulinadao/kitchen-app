﻿namespace Paulina.Kitchen
{
    using System;
    using System.IO;
    using System.Collections.Generic;

    public class FileItemRepository : IItemRepository
    {
        /// <summary>
        /// Gets the filename.
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileItemRepository"/> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        public FileItemRepository(string filename)
        {
            this.Filename = filename;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            using (var stream = File.Open(this.Filename, FileMode.OpenOrCreate))
            {
            }
        }

        /// <summary>
        /// Updates or inserts the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Upsert(Item item)
        {

            if (string.IsNullOrWhiteSpace(item.Name))
            {
                throw new ArgumentNullException();
            }

            if(string.IsNullOrWhiteSpace(Convert.ToString(item.Measurement.Quantity)))
            {
                throw new ArgumentNullException("quantity");
            }

            if(string.IsNullOrWhiteSpace(Convert.ToString(item.Measurement.MeasurementType)))
            {
                throw new ArgumentNullException("measurement");
            }

            var lines = File.ReadAllLines(this.Filename);
            List<string> newlines = new List<string>();
            bool seenItem = false;

            foreach(var line in lines)
            {
                var linearray = line.Split(',');


                if (linearray[0] == item.Name)
                {
                    linearray[1] = Convert.ToString(item.Measurement.Quantity);

                    linearray[2] = Convert.ToString(item.Measurement.MeasurementType);

                    string stringfromlinearray = string.Join(",", linearray); 

                    newlines.Add(stringfromlinearray);

                } 
                else
                {
                    newlines.Add(line);
                }

                seenItem = true;
            }
            if (seenItem == false)
            {
                string[] newarray = new string[3];
                newarray[0] = item.Name;
                newarray[1] = Convert.ToString(item.Measurement.Quantity);
                newarray[2] = Convert.ToString(item.Measurement.MeasurementType);

                string stringfromarray = string.Join(",", newarray);

                newlines.Add(stringfromarray);
            }
            File.WriteAllLines(this.Filename, newlines);
        }

        /// <summary>
        /// Gets the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>
        /// An item with the specified name.
        /// </returns>
        public Item Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("name");
            }

            var lines = File.ReadAllLines(this.Filename);
            for (int line = 0; line < lines.Length; line++)
            {
                var linearray = lines[line].Split(',');

                if (linearray[0] == name)
                {
                    var item = new Item();
                    item.Name = linearray[0];
                    item.Measurement = new Measurement();
                    item.Measurement.Quantity = float.Parse(linearray[1]);
                    item.Measurement.MeasurementType =
                        (MeasurementType)Enum.Parse(typeof(MeasurementType), linearray[2]);

                    return item;
                }
            }

            return null;
        }
    }
}