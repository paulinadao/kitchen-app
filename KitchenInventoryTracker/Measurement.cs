﻿namespace Paulina.Kitchen
{
    public class Measurement
    {
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        public float Quantity { get; set; }

        /// <summary>
        /// Gets or sets the type of the measurement.
        /// </summary>
        /// <value>
        /// The type of the measurement.
        /// </value>
        public MeasurementType MeasurementType { get; set; }
    }
}