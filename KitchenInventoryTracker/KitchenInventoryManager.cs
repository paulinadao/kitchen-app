﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paulina.Kitchen
{
    public class KitchenInventoryManager
    {
        /// <summary>
        /// The repository.
        /// </summary>
        private readonly IItemRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="KitchenInventoryManager"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public KitchenInventoryManager(IItemRepository repository)
        {
            this.repository = repository;
        }
    }
}
